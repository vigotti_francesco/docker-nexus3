#!/usr/bin/env bash
# this script will build into /usr/bin/app/build

# run with:
# buildJar.sh
# or
# buildJar.sh test build ...

set -x

export SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )

# constants
readonly export TAG_NAME_TMP_FILE='build_vars/tag.name' # this file will contain generated version name
readonly export BRANCH_NAME_TMP_FILE='build_vars/branch.name' # this file will contain generated version name
readonly export VERSION_NAME_TMP_FILE='build_vars/version.name' # this file will contain generated version name
readonly export BUILD_RESULTS_TMP_FILE='build_vars/build.results' # this file will contain generated version name

mkdir -p "${PROJECT_DIR}/build_vars/"


## there are needed because commit will be cherry picked from jenkins plugin so easier ways to retrieve those informations do not work
getShortCommitHash(){
  echo $(git rev-parse --short HEAD)
}

getCurrentBranch(){
  echo $(git for-each-ref --sort=-committerdate --format='%(refname:short) %(objectname)' | grep "$(git rev-parse HEAD)"  | head -1 | cut -d" " -f 1 | cut -d '/' -f 2)
  # | grep origin
}

# ie: 20160729.153549
buildVersionDateTag(){
  date +'%Y%m%d.%H%M%S'
}

getCurrentTag(){
  git describe --exact-match --tags $(git rev-parse HEAD) 2>/dev/null || printf ''
}


appendBuildResults(){
 KEY=$1
 VALUE=$2
 echo ${KEY}'='$VALUE >> "${PROJECT_DIR}/$BUILD_RESULTS_TMP_FILE"
}
initBuildResultsFile(){
 echo '# build results initialized '$(date) > "${PROJECT_DIR}/$BUILD_RESULTS_TMP_FILE"
 appendBuildResults 'buildDate' "'$(date)'"
}

getBuildRepoFullDigest(){ # return the full repository digest ( --> repoUrl/imageName@sha256:checksum <-- )
    IMAGEID=$1
    docker inspect  --format='{{ (index .RepoDigests 0) }}'  $IMAGEID
}
#
#getCurrentVersion(){
#  if [ -f "${PROJECT_DIR}/VERSION" ]; then
#  head -1  "${PROJECT_DIR}/VERSION" | cut -f 1
#  else
#  printf $DEFAULT_EMPTY_VERSION
#  fi
#}

## if current branch is dev, append "-SNAPSHOT" TO VERSION-
#getCurrentBuildVersion(){
#  if [ -f "${PROJECT_DIR}/VERSION" ]; then
#  version=$(head -1  "${PROJECT_DIR}/VERSION" | cut -f 1)
#  version_append=""
#  if [ "$(getCurrentBranch)" != "master" ]; then
#    export version_append="-SNAPSHOT"
#  fi
#  echo "${version}${version_append}"
#  else
#  printf $DEFAULT_EMPTY_VERSION
#  fi
#}




