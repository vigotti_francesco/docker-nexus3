# usage:


## branch building and versioning:
- git branch `master` -> will build `:master` and tagged version if tag is present -> `:$(git current tag )` 
- git branch`dev` -> will build to :latest


  ## prepare:
  - add `build_vars/` to gitignore
  - pull fresh scripts
    ```bash
    docker pull quay.io/fravi/ci-imagebuilder:latest
    OWNERID=$(stat -c '%u' ./)
    docker run --rm -ti -e OWNERID=${OWNERID} -v `pwd`/ci:/target quay.io/fravi/ci-imagebuilder:latest
    ```

   
  ## step 1: extract tag and version from git 
  ./ci/10_getProjectVersion.sh
    
  will create 3 files that can be read ( ie by jenkins to generate
  build informations )
   - build_vars/tag.name # this file will contain generated version name
   - build_vars/branch.name # this file will contain generated version name
   - build_vars/version.name # this file will contain generated version name
   - build_vars/build.results # this file will contain repository-digest (as $IMAGE_DIGEST) after image push to repository

  to read those files with jenkins use readfile :  
  `def tagname = readFile 'build_vars/tag.name'`
      
  ## step 2:
   build the image  
   `$$$ARGS$$$ ./ci/50_buildImage.sh`  
   ie:   
   `DOCKER_REPO="quay.io/fravi" PUSH=1 IMAGENAME="images-ci-scripts" ./ci/50_buildImage.sh`
   
             
 | ARGS| defined as env vars |
 |---|---|
 | $DOCKER_BUILD_OPTS | additional build options, ie: DOCKER_BUILD_OPTS="--network=host --compress --squash" |
 | $IMAGENAME | imagename ( default to directory name )  |
 | $PUSH | push the image to repository after build  |
 | $DOCKERFILE | full path of the dockerfile, default to `$projectDir/src/Dockerfile` , fallback to `$projectDir/Dockerfile`  |  
 | $REFRESH_BASEIMAGE | default 1 , if 1 will attempt to pull the base image `FROM` from specified Dockerfile before  building |  
 | ${DOCKER_NETBUILD_OPTIONS:-} | the same as $DOCKER_NETOPTIONS , but used in `docker build` commands ( require docker >= 1.13 )        
     

### to update those scripts:
from project directory
```bash
# docker tags :  
# :master - to get last master
# :latest - to get last dev 
# :vxxx - to get specific version 

docker pull quay.io/fravi/ci-imagebuilder:master
OWNERID=$(stat -c '%u' ./)
docker run --rm -ti -e OWNERID=${OWNERID} -v `pwd`/ci:/target quay.io/fravi/ci-imagebuilder:master
```
