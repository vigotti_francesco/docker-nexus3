#!/usr/bin/env bash
set -xe



export SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )
export BASENAME=$(basename $PROJECT_DIR)


. $SCRIPT_DIR/05_SHARED_LIB.sh

## ACCEPTED ENV VARIABLES :
export DOCKER_REPO=${DOCKER_REPO:-test}
export PUSH=${PUSH:-0} # IF 1 , image will be pushed to docker repository
export IMAGENAME=${IMAGENAME:-$BASENAME} # IF 1 , image will be pushed to docker repository

export REFRESH_BASEIMAGE=${REFRESH_BASEIMAGE:-1} # IF 1 , image will be pushed to docker repository
export TAG_VERSION=$(getCurrentTag)

if [ -z "${DOCKERFILE}" ]; then
  export DOCKERFILE="${PROJECT_DIR}/src/Dockerfile" # default to /src/Dockerfile
  if [ ! -f "${DOCKERFILE}" ] && [ -f "${PROJECT_DIR}/Dockerfile" ]; then # but if not exist
    export DOCKERFILE=$PROJECT_DIR"/Dockerfile" ## default to /Dockerfile
  fi
fi

if [ ! -f "${DOCKERFILE}" ]; then
  echo "[FATAL] DOCKERFILE NOT FOUND at path : ${DOCKERFILE}"
  exit 1 ;
fi

## enter the directory of the dockerfile ( all COPY / ADD / .. command should reference from Dockerfile location )
cd $(dirname "${DOCKERFILE}")


if [ -z "${IMAGENAME}" ]; then
  echo " please provide imagename! "
  exit 1;
fi

DOCKER_BUILD_OPTS=${DOCKER_BUILD_OPTS:-} # ie : --network=host

initBuildResultsFile

function do_refresh_base_image(){
  BASEIMAGE=$(cat ${DOCKERFILE}  | egrep "^FROM" | cut -d' ' -f 2)
  if [ ! -z "${BASEIMAGE}" ]; then
   docker pull "${BASEIMAGE}" || echo "[WARNING] error refreshing base image ${BASEIMAGE} before build "
  fi
}
 [[ ${REFRESH_BASEIMAGE} -eq 1 ]] && do_refresh_base_image


function do_build(){
  DOCKER_REPO=$1
  IMAGENAME=$2
  TAG=$3
  docker build ${DOCKER_NETBUILD_OPTIONS:-} ${DOCKER_BUILD_OPTS} -t ${DOCKER_REPO}/${IMAGENAME}:${TAG} -f "${DOCKERFILE}" .
  echo "[BUILT]" ${DOCKER_REPO}/${IMAGENAME}:${TAG}
}

function do_retag(){
  DOCKER_REPO=$1
  IMAGENAME=$2
  TAG=$3
  DOCKER_REPO_NEW=$4
  IMAGENAME_NEW=$5
  TAG_NEW=$6
  docker tag ${DOCKER_REPO}/${IMAGENAME}:${TAG} ${DOCKER_REPO_NEW}/${IMAGENAME_NEW}:${TAG_NEW}
  echo "[TAGGED]" ${DOCKER_REPO_NEW}/${IMAGENAME_NEW}:${TAG_NEW}
}

function do_push(){
  DOCKER_REPO=$1
  IMAGENAME=$2
  TAG=$3

  _full_image="${DOCKER_REPO}/${IMAGENAME}:${TAG}"
  docker push $_full_image
  echo "[PUSHED]" $_full_image
  #appendBuildResults "IMAGE_TAG_NAME" "${TAG}" # ie IMAGE_TAG_NAME=latest
  #appendBuildResults "IMAGE_TAG_${TAG}_DIGEST" "$(getBuildRepoFullDigest $_full_image)" # IMAGE_TAG_latest_DIGEST=$DIGEST ( not valid because tagname could contain invalid char for variable name ( ie : '.' ))

  appendBuildResults "IMAGE_NAME" "$_full_image"
  appendBuildResults "IMAGE_DIGEST" "$(getBuildRepoFullDigest $_full_image)"
}

## if on master branch  build :master and :v$tag
if [ "$(getCurrentBranch)" = "master" ]; then
 do_build "${DOCKER_REPO}" "${IMAGENAME}" "master"
 [[ ${PUSH} -eq 1 ]] && do_push "${DOCKER_REPO}" "${IMAGENAME}" "master"

 # if git tag is defined build with :$tag
 [[ ! -z "${TAG_VERSION}" ]] && do_retag "${DOCKER_REPO}" "${IMAGENAME}" "master" "${DOCKER_REPO}" "${IMAGENAME}" "${TAG_VERSION}"
 [[ ${PUSH} -eq 1 ]] && [[ ! -z "${TAG_VERSION}" ]] && do_push "${DOCKER_REPO}" "${IMAGENAME}" "${TAG_VERSION}"
fi

## if on dev branch build :latest
if [ "$(getCurrentBranch)" = "dev" ]; then
 do_build "${DOCKER_REPO}" "${IMAGENAME}" "latest"
 [[ ${PUSH} -eq 1 ]] && do_push "${DOCKER_REPO}" "${IMAGENAME}" "latest"
fi

exit 0 ; ## ensure that this is the last errorcode if the program reach this point 