## TODO
problem with new version of nexus during the execution of `docker-entrypoint.sh` , something is wrong,
ssl is not activable 

# nexus3 dockerized
the official image : https://github.com/sonatype/docker-nexus3
lacks some important configuration that cannot be done during first container bootrap because 
user is changed to `nexus`

this container is better: 
    - https://github.com/clearent/nexus
    - https://github.com/frekele/docker-nexus
    
but alpine image could be a bit restrictive,
and I have no control on this repo , nor is official  ,and have some parts that could be an issue in kubernetes ( ie : /keystore.jks have to be mounted as file )
 
# test image : 
```
docker build . -t nex    

mkdir -p /tmp/nextest
cd /tmp/nextest
export JKS_PASSWORD=changeit
export JKS_FILEPATH="/nexus-ssl/keystore.jks"

## build jks  
export NEXUS_IP="127.0.0.1"
export NEXUS_DOMAIN="reg"
export SSL_PORT="8443"

keytool -genkeypair -keystore keystore.jks -storepass ${JKS_PASSWORD} -keypass ${JKS_PASSWORD} -alias jetty -keyalg RSA -keysize 2048 -validity 5000 -dname "CN=*.${NEXUS_DOMAIN?}, OU=Example, O=Sonatype, L=Unspecified, ST=Unspecified, C=US" -ext "SAN=DNS:${NEXUS_DOMAIN?},IP:${NEXUS_IP?}" -ext "BC=ca:true"

docker run --rm -ti  -v  /tmp/nextest:/nexus-ssl/  \
-p127.0.0.1:8081:8081 -p127.0.0.1:8083:8083 -p127.0.0.1:8443:8443 -p127.0.0.1:8444:8444 nex

## test image built with jenkins:
docker pull quay.io/fravi/docker-nexus3:master
docker run --rm -ti  -v  /tmp/nextest:/nexus-ssl/  \
-p127.0.0.1:8081:8081 -p127.0.0.1:8083:8083 -p127.0.0.1:8443:8443 -p127.0.0.1:8444:8444 quay.io/fravi/docker-nexus3:master
# then open : http://127.0.0.1:8081

```

## available ENV VARS for configuration:
 - JKS_PASSWORD = password for jks file 
 - JAVA_MIN_HEAP = -Xms value, default: 1200m 
 - JAVA_MAX_HEAP = -Xmx value  default: 1200m
 
## storage :
 - /nexus-ssl/ = directory where jks file exist this directory is dedicated to keystore.jks (allow mount secrets in folder in kubernetes ) ie: /nexus-ssl/keystore.jks
 - /nexus-data = default directory where nexus stores everything, ( be aware that this will contain both blobs and runtime configuration ) so backups should be careful 
